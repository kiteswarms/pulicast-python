image: registry.gitlab.com/kiteswarms/docker-ks-build-env/ubuntu20.04/ks-build-env:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  # Change pip's cache directory to be inside the project directory since we can
  # only cache local items.
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

stages:
  - changelogtest
  - configure
  - build
  - test
  - analysis
  - docs
  - upload

tox_pytest_coverage_job38:
    stage: test
    tags:
        - docker
    rules:
        # The following two rules make sure the job is always executed.
        - if: '$CI_COMMIT_BRANCH == "develop"'
        - if: '$CI_COMMIT_BRANCH == "master"'
        - if: $CI_COMMIT_TAG
        - if: $CI_MERGE_REQUEST_ID
        - if: '$CI_PIPELINE_SOURCE == "web"'
    before_script:
        - python3 -V               # Print out python version for debugging
        - pip3 install -U pip wheel setuptools
    script:
        - pip3 install tox
        - tox 
    artifacts:
        expire_in: 1 days
        paths:
            - htmlcov
        reports:
            junit: report.xml
            cobertura: coverage.xml
    needs: []



# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  paths:
    - .cache/pip
  key: ${CI_PROJECT_ID}


changelogtest_job:
  stage: changelogtest
  image: python:3.8
  tags:
    - docker
  before_script:
    - echo "Check that Changelog has been modified"
  script:
    - git fetch --all
    - if [ 0 -eq $(git diff --name-only origin/${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}...${CI_COMMIT_SHA} CHANGELOG.md | wc -l) ]; then exit 1; fi
  only:
    - merge_requests
  needs: []
  dependencies: []


sphinx-docs:
    stage: docs
    tags:
        - docker
    rules:
        # The following two rules make sure the job is always executed.
        - if: '$CI_COMMIT_BRANCH == "develop"'
        - if: '$CI_COMMIT_BRANCH == "master"'
        - if: $CI_COMMIT_TAG
        - if: $CI_MERGE_REQUEST_ID
        - if: '$CI_PIPELINE_SOURCE == "web"'
    before_script:
        - python3 -V               # Print out python version for debugging
        - pip3 install -U pip wheel setuptools
    script:
        - pip3 install recommonmark sphinx
        - pip3 install anybadge
        - pip3 install -r docs-requirements.txt || echo "no additional requirements for docs specified"
        - pip3 install .
        - mkdir public
        - sphinx-apidoc -f -o docs/source src
        - make -C docs html || exit_code=$?
        - >
          if [[ $exit_code -ne 0 ]]; then
            echo "Sphinx-build failure"
            anybadge -l docs -v failed -f docs.svg -c red
            exit 1
          else
            echo "Sphinx-build success"
            anybadge -l docs -v build -f docs.svg -c green
            mv docs/build/html/* public
          fi
    artifacts:
        paths:
            - docs.svg
            - public
        when: always
    needs: []


pages:
  image: alpine:latest
  stage: upload
  tags:
    - docker
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+$/ && $CI_COMMIT_REF_PROTECTED == "true"'
  before_script:
    - echo 'Nothing to do...'
  script:
    - echo 'Nothing to do...'
  artifacts:
    paths:
        - docs.svg
        - public

  needs: ["sphinx-docs"]

build-and-test-pip-package3.8:
    rules:
        # The following two rules make sure the job is always executed.
        - if: '$CI_COMMIT_BRANCH == "develop"'
        - if: '$CI_COMMIT_BRANCH == "master"'
        - if: $CI_COMMIT_TAG
        - if: $CI_MERGE_REQUEST_ID
        - if: '$CI_PIPELINE_SOURCE == "web"'
    stage: build
    tags:
        - docker
    before_script:
        - pip3 install twine
    script:
        - python3 setup.py sdist bdist_wheel
        # Check sdist and wheel package integrity
        - twine check dist/*
    artifacts:
        expire_in: 1 day
        paths:
            - dist/
    needs: []


publish-pip-package:
    image: python:3.8
    stage: upload
    rules:
        - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+$/ && $CI_COMMIT_REF_PROTECTED == "true"'
    tags:
        - docker
    before_script:
        - pip install twine
    script:
          # Upload to kiteswarms.  Requires that the following variables be set:
          #  1.   $TWINE_USERNAME           the username to use for authentication to the repository.
          #  2.   $TWINE_PASSWORD           the password to use for authentication to the repository.
        - twine upload dist/*
