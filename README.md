# Pulicast Python

The python implementation of pulicast. 
Documentation can be found [here](https://kiteswarms.gitlab.io/pulicast-python/) and in the 
[examples](./examples) directory.

## Installation
Install the library via pip:

```bash
pip3 install pulicast
```
or pulicast tools using pipx:
```bash
pipx install "pulicast[tools]"
```

## Documentation
For an overview of the basic principles refer to the [readme of the C++ repository](https://gitlab.com/kiteswarms/pulicast-cpp#installation-and-usage) for now.
There is a number of examples in the `examples/` directory.
The documentation can be found [here](https://kiteswarms.gitlab.io/pulicast-python/).
[This whitepaper](https://gitlab.com/kiteswarms/pulicast-cpp/-/blob/master/Distributed%20Properties%20with%20Pulicast.md) explains the principles of the property implementation.
